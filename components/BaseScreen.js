//import * as React from 'react';
import React, { Component } from 'react';



export default class Store extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
        { id: 1, title: "Product 1", price: "$ 25.00 USD", image: "https://images-na.ssl-images-amazon.com/images/I/51819dyF6gL._SX336_BO1,204,203,200_.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 2, title: "Product 2", price: "$ 10.13 USD", image: "https://lh3.googleusercontent.com/proxy/BiE3f0wg5Mxy9FFbYO69vtYc8rU1wRb0SysNbpqdzHVQbxSx7bns9jq1E1mlCFqu2vy1b4GtrYxLGd7wqA6NanNbkFKZkgXpK2hqnrD9VorLepcAJitYb7bljyNhMSL4MfUzPOkeZDYGkP9mxzPoqA", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 3, title: "Product 3", price: "$ 12.12 USD", image: "https://media.senscritique.com/media/000000012466/source_big/Harry_Potter_et_le_Prisonnier_d_Azkaban.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 4, title: "Product 4", price: "$ 11.00 USD", image: "https://images-na.ssl-images-amazon.com/images/I/81tjfAVpVcL.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 5, title: "Product 5", price: "$ 20.00 USD", image: "https://images-na.ssl-images-amazon.com/images/I/51npM+uuy2L._SX331_BO1,204,203,200_.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 6, title: "Product 6", price: "$ 33.00 USD", image: "http://static.fnac-static.com/multimedia/images_produits/ZoomPE/0/7/6/9782070572670.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 7, title: "Product 7", price: "$ 20.95 USD", image: "https://images-na.ssl-images-amazon.com/images/I/71Xz6t1dAJL.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 8, title: "Product 8", price: "$ 13.60 USD", image: "https://static.fnac-static.com/multimedia/Images/FR/NR/f8/cb/9f/10472440/1507-0/tsp20191026071304/Les-animaux-fantastiques.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 9, title: "Product 9", price: "$ 15.30 USD", image: "https://static.fnac-static.com/multimedia/Images/FR/NR/13/23/9b/10167059/1507-1/tsp20201003073751/Les-Crimes-de-Grindelwald.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
        { id: 10, title: "Product 10", price: "$ 21.30 USD", image: "https://images-na.ssl-images-amazon.com/images/I/81DEjdyCwEL.jpg", description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati" },
      ]
    };
  };
};